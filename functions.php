
<?php


/**
 * Sets up theme defaults WordPress.
 */
if ( ! function_exists( 'test_less_setup' ) ) :

function test_less_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on test_less, use a find and replace
	 * to change 'test_less' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'test_less', get_template_directory() . '/languages' );
   
   
   // Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );


	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );


	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 750, 300 );



	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus(
		array(
			'mainNav'    => 'Верхнее меню',    //Название месторасположения меню в шаблоне
			'bottom' => 'Нижнее меню'   
		)
	);

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );



} // END --- test_less_setup() ---

endif;
add_action( 'after_setup_theme', 'test_less_setup' );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function test_less_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'test_less' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'test_less' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'test_less_widgets_init' );





// add_filter('navigation_markup_template', 'my_navigation_template', 10, 2 );
// function my_navigation_template( $template, $class ){
	
// 	Вид базового шаблона:
// 	<nav class="navigation %1$s" role="navigation">
// 		<h2 class="screen-reader-text">%2$s</h2>
// 		<div class="nav-links">%3$s</div>
// 	</nav>
	

// 	return '
// 	<nav aria-label="My Nav" role="navigation">
// 		%3$s
// 	</nav>    
// 	';
// }


// Main Mune Castom =====

// // // Изменяет основные параметры меню
add_filter( 'wp_nav_menu_args', 'filter_wp_menu_args' );
function filter_wp_menu_args( $args ) {
	if ( $args['theme_location'] === 'mainNav' ) {

			$args['container']  = 'div';
			$args['container_class']  = 'collapse navbar-collapse';
			$args['container_id']  = 'main_navbarResponsive';
			//$args['items_wrap'] = '<ul class="%2$s">%3$s</ul>';
			$args['menu_class'] = 'navbar-nav ml-auto';
			$args['menu_id'] = 'mainNav';
	}

	return $args;
}

// Изменяем атрибут id у тега li
// add_filter( 'nav_menu_item_id', 'filter_menu_item_css_id', 10, 4 );
// function filter_menu_item_css_id( $menu_id, $item, $args, $depth ) {

// 	if ( $args->theme_location === 'mainNav' ) {
		
// 		if ( $item->menu_order === 1 ) {
// 			$menu_id = 'firstLink';	
// 		}
// 		else {
// 			$menu_id = '';
// 		}
// 	}
// 	return $menu_id;	
// }

// Изменяем атрибут class у тега li
add_filter( 'nav_menu_css_class', 'filter_nav_menu_css_classes', 10, 4 );
function filter_nav_menu_css_classes( $classes, $item, $args, $depth ) {
	if ( $args->theme_location === 'mainNav' ) {

		$classes = ['nav-item'];
       
		if ( $item->current ) {
			$classes[] = 'active';
		}
	}

	return $classes;
}

 // Изменяет класс у вложенного ul
add_filter( 'nav_menu_submenu_css_class', 'filter_nav_menu_submenu_css_class', 10, 3 );
function filter_nav_menu_submenu_css_class( $classes, $args, $depth ) {
	if ( $args->theme_location === 'mainNav' ) {
		$classes = [
			'menu',
			'menu--dropdown',
			'menu--vertical'
		];
	}

	return $classes;
}


// ДОбавляем классы ссылкам
add_filter( 'nav_menu_link_attributes', 'filter_nav_menu_link_attributes', 10, 4 );
function filter_nav_menu_link_attributes( $atts, $item, $args, $depth ) {
	if ( $args->theme_location === 'mainNav' ) {
		$atts['class'] = 'nav-link';
	}

	return $atts;
}



// // Remove themes old version of jQuery and load a compatible version
// function my_update_jquery () {
// 	if ( !is_admin() ) { 
// 	   wp_deregister_script('jquery');
//  	   wp_register_script('jquery', get_theme_file_uri('/assets/js/jquery-1.8.2.min.js'), false, false, true);
// 	   wp_enqueue_script('jquery');
// 	}
// }
// add_action('wp_enqueue_scripts', 'my_update_jquery');


/**
 * Enqueues scripts and styles.

 D:\laragon\www\myBlog.loc\wp-content\themes\lessontheme\assets\css
 */

function test_less_scripts() {
	
	// Theme stylesheet.
    // <!-- Bootstrap core CSS -->
	wp_enqueue_style( 'test_less-bootstrap-style', get_theme_file_uri( '/assets/vendor/bootstrap/css/bootstrap.min.css' ) );
	
	//<!-- Custom styles for this template -->
	wp_enqueue_style( 'test_less-style', get_stylesheet_uri() );


	// Theme script.


    wp_enqueue_script('jquery');

    wp_enqueue_script( 'test_less-bootstrap-js', get_theme_file_uri( '/assets/vendor/bootstrap/js/bootstrap.bundle.min.js' ), array('jquery'), null, true );
    
}
add_action( 'wp_enqueue_scripts', 'test_less_scripts' );




// // Register Custom Navigation Walker
// require_once('wp_bootstrap_pagination.php');



// function customize_wp_bootstrap_pagination($args) {
    
//     $args['range'] = 3;
//     $args['previous_string'] = '<<';
//     $args['next_string'] = '>>';
//     $args['before_output'] = '<nav aria-label="Post navigation example"><ul class="pagination justify-content-center">';
//     $args['after_output'] = '</ul></nav>';
    
//     return $args;
// }
// add_filter('wp_bootstrap_pagination_defaults', 'customize_wp_bootstrap_pagination');



// Работа с отрывком текста 
add_action( 'the_excerpt', 'modify_the_excerpt' );

function modify_the_excerpt( $post_excerpt ) {
	return '<p class="card-text">' . $post_excerpt . '</p>';
}


function new_excerpt_more( $more ){
	// global $post;
	// //<a href="#" class="btn btn-primary">Read More &rarr;</a>
	return '';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );


add_filter( 'excerpt_length', function(){
	return 30;
} );





?>