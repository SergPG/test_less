<?php
/**
 * The main template file
 *
 * 
 */


?>

	

<?php 
   get_header(); 
 ?>
     
     <?php echo "index.php"; ?>


<?php if ( !is_home() &&  is_front_page() ) : ?>

    <h1 class="my-4">Welcome to Static Front Page Site</h1>

    <?php elseif ( is_home()) : ?>

     <h1 class="my-4">Welcome to Lists Posts - Home Page Site</h1>

     <?php else : ?>

        <h1 class="my-4">Welcome to Single Page Site</h1>

  <?php endif; ?>

    <!-- Page Heading/Breadcrumbs -->
    <!-- <h1 class="mt-4 mb-3">Blog Home One
      <small>Subheading</small>
    </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Blog Home 1</li>
      </ol>-->

    <div class="row"> 

      <!-- Blog Entries Column -->
      <div class="col-md-8">
     
        <?php if ( have_posts() ) : ?>	
             <?php  
             	// Start the loop.
        		while ( have_posts() ) :
        			the_post();
              ?>
  
              <?php  get_template_part( 'template-parts/content/content' ); ?>

              <?php                       
                // End the loop.
        		endwhile;
        ?>

  
    <?php
      if ( function_exists('wp_bootstrap_pagination') )
         wp_bootstrap_pagination();
    ?>

    <?php  endif ?> 


     </div>
        
  

<?php 
  get_sidebar();
  get_footer(); 
?>


