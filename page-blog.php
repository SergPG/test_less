<?php
/**
 * The Page Blog template file
 */

?>

	

<?php 
   get_header(); 
 ?>

     <?php echo "page-blog.php"; ?>

    <!-- Page Heading/Breadcrumbs -->
    <h1 class="mt-4 mb-3">Blog Home One
      <small>Subheading</small>
    </h1>

      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="index.html">Home</a>
        </li>
        <li class="breadcrumb-item active">Blog Home 1</li>
      </ol>

    <div class="row">

      <!-- Blog Entries Column -->
      <div class="col-md-8">
     


<?php
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$args = array( 'paged' => $paged ,'post_type' => 'post', 'posts_per_page' => '4' );
 
$loop = new WP_Query( $args );
  while ( $loop->have_posts() ) :
      $loop->the_post(); 
?>
 
 <?php  get_template_part( 'template-parts/content/content' ); ?>
 
<?php
  endwhile;
    wp_reset_postdata();
?>


<?php
    //  $pll =    get_pagenum_link(2);

      $first_page = 1;
      $last_page = (int)$loop->max_num_pages;

      $current_page = intval( get_query_var( 'paged' ) );

      if (!$current_page){
          $older_page = $first_page + 1;
      }
      else{
          $older_page = $current_page + 1;
      }

     // var_dump($current_page );
     // var_dump($older_page );
     // var_dump($link_older_page );
?>

<!-- Pagination disabled-->
  <ul class="pagination justify-content-center mb-4">
     <?php  if( !$current_page): ?>
      <li class="page-item disabled">
        <a class="page-link " href="#">&larr; Newer</a>
      </li>
     <?php else: ?>
        <li class="page-item">
        <a class="page-link " href="<?php  echo esc_attr( get_pagenum_link($current_page - 1) ); ?>">&larr; Newer</a>
      </li>
     <?php endif; ?>

     <?php  if( $current_page == $last_page): ?>
      <li class="page-item disabled">
        <a class="page-link " href="#">Older &rarr;</a>
      </li>
     <?php else: ?>
        <li class="page-item">
        <a class="page-link " href="<?php echo esc_attr( get_pagenum_link($older_page) ); ?>">Older &rarr;</a>
      </li>
     <?php endif; ?>           
  </ul>

  

   

     </div>
        
  

<?php 
  get_sidebar();
  get_footer(); 
?>


