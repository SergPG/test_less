<?php
/**
 * Template part for displaying posts
 *
 */

?>

	<!-- Blog Post -->
    <div class="card mb-4">

        <?php if ( has_post_thumbnail() ) {
                   the_post_thumbnail(
                      array(750, 300),
                      array(
                            'class' => "card-img-top",
                            'alt' => 'Card image cap'
                            )
                   );
        } ?>
 
      <div class="card-body">

        <?php the_title( sprintf( '<h2 class="card-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
          
         <?php  the_excerpt();  ?> 
       
        <a href="<?php the_permalink(); ?>" class="btn btn-primary">Read More</a>
      </div>

      <div class="card-footer text-muted">
        Posted on <?php the_time('j F Y'); ?>  by
        <a href="#"><?php the_author(); ?></a>
      </div>
    </div> 

